FROM openjdk:8-jdk-alpine

VOLUME [ "/tmp" ]

COPY build/libs/rental-appl-0.0.1-SNAPSHOT.jar dvdrental.jar

CMD ["java", "-jar", "dvdrental.jar"]