package com.dvd.rental.rentalappl.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class FilmDto {
  private Long filmId;
  private String title;
  private String description;
  private String releaseYear;
  private LanguageDto language;
  private Integer rentalDuration;
  private Double rentalRate;
  private Integer length;
  private Double replacementCost;
  private String rating;
  private Date lastUpdate;
  private String specialFeatures;
  private String fulltext;
}