package com.dvd.rental.rentalappl.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class CustomerDto {
  private Long customerId;
  private String firstName;
  private String lastName;
  private String email;
  private AddressDto address;
  private Boolean activebool;
  private Date createDate;
  private Date lastUpdate;
  private Integer active;
}