package com.dvd.rental.rentalappl.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class ActorInfoDto {
  private Long actorId;
  private String firstName;
  private String lastName;
  private String filmInfo;
}