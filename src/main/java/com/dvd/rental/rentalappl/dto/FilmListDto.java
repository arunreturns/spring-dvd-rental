package com.dvd.rental.rentalappl.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class FilmListDto {
  private Long fid;
  private String title;
  private String description;
  private String category;
  private Double price;
  private Integer length;
  private String rating;
  private String actors;
}