package com.dvd.rental.rentalappl.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class AddressDto {
  private Long addressId;
  private String address;
  private String address2;
  private String district;
  private CityDto city;
  private String postalCode;
  private String phone;
  private Date lastUpdate;
}