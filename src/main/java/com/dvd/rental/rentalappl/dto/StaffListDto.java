package com.dvd.rental.rentalappl.dto;

import javax.persistence.Column;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class StaffListDto {
  private Long id;
  private String name;
  private String address;
  @Column(name = "\"zip code\"")
  private String zipCode;
  private String phone;
  private String city;
  private String country;
  private Long sid;
}