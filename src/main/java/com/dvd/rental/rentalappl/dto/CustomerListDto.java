package com.dvd.rental.rentalappl.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class CustomerListDto {
  private Long id;
  private String name;
  private String address;
  private String zipCode;
  private String phone;
  private String city;
  private String country;
  private String notes;
  private Long sid;
}