package com.dvd.rental.rentalappl.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class StaffDto {
  private Long staffId;
  private String firstName;
  private String lastName;
  private AddressDto address;
  private String email;
  private StoreDto store;
  private Boolean active;
  private String username;
  private String password;
  private Date lastUpdate;
  private String picture;
}