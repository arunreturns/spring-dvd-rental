package com.dvd.rental.rentalappl.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class FilmActorDto {
  private Long actorId;
  private Long filmId;
  private Date lastUpdate;
}