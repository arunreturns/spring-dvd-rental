package com.dvd.rental.rentalappl.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class RentalDto {
  private Long rentalId;
  private Date rentalDate;
  private Long inventoryId;
  private Long customerId;
  private Date returnDate;
  private Long staffId;
  private Date lastUpdate;
}