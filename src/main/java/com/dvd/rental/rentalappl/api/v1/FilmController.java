
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.FilmRepository;
import com.dvd.rental.rentalappl.dto.FilmDto;
import com.dvd.rental.rentalappl.mapper.FilmMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/film")
public class FilmController {

  private FilmRepository filmRepositroy;

  @Autowired
  public FilmController(FilmRepository filmRepositroy) {
    this.filmRepositroy = filmRepositroy;
  }

  @GetMapping("/")
  public List<FilmDto> getItems() {
    return FilmMapper.mapFilms(filmRepositroy.findAll());
  }
}