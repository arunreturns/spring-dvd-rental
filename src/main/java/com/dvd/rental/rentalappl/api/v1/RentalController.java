
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.RentalRepository;
import com.dvd.rental.rentalappl.dto.RentalDto;
import com.dvd.rental.rentalappl.mapper.RentalMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rental")
public class RentalController {

  private RentalRepository rentalRepositroy;

  @Autowired
  public RentalController(RentalRepository rentalRepositroy) {
    this.rentalRepositroy = rentalRepositroy;
  }

  @GetMapping("/")
  public List<RentalDto> getItems() {
    return RentalMapper.mapRentals(rentalRepositroy.findAll());
  }
}