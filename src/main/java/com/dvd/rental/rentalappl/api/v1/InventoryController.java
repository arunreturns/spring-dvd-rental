
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.InventoryRepository;
import com.dvd.rental.rentalappl.dto.InventoryDto;
import com.dvd.rental.rentalappl.mapper.InventoryMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/inventory")
public class InventoryController {

  private InventoryRepository inventoryRepositroy;

  @Autowired
  public InventoryController(InventoryRepository inventoryRepositroy) {
    this.inventoryRepositroy = inventoryRepositroy;
  }

  @GetMapping("/")
  public List<InventoryDto> getItems() {
    return InventoryMapper.mapInventories(inventoryRepositroy.findAll());
  }
}