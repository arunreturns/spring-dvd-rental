
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.StoreRepository;
import com.dvd.rental.rentalappl.dto.StoreDto;
import com.dvd.rental.rentalappl.mapper.StoreMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/store")
public class StoreController {

  private StoreRepository storeRepositroy;

  @Autowired
  public StoreController(StoreRepository storeRepositroy) {
    this.storeRepositroy = storeRepositroy;
  }

  @GetMapping("/")
  public List<StoreDto> getItems() {
    return StoreMapper.mapStores(storeRepositroy.findAll());
  }
}