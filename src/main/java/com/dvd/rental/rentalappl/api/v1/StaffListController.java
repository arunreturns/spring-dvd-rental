package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.StaffListRepository;
import com.dvd.rental.rentalappl.dto.StaffListDto;
import com.dvd.rental.rentalappl.mapper.StaffListMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/staffList")
public class StaffListController {

  private StaffListRepository staffListRepository;

  @Autowired
  public StaffListController(StaffListRepository staffListRepository) {
    this.staffListRepository = staffListRepository;
  }

  @GetMapping("/")
  public List<StaffListDto> getItems() {
    return StaffListMapper.mapStaffLists(staffListRepository.findAll());
  }
}