package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.CustomerRepository;
import com.dvd.rental.rentalappl.dto.CustomerDto;
import com.dvd.rental.rentalappl.mapper.CustomerMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {

  private CustomerRepository customerRepository;

  @Autowired
  public CustomerController(CustomerRepository customerRepository) {
    this.customerRepository = customerRepository;
  }

  @GetMapping("/")
  public List<CustomerDto> getItems() {
    return CustomerMapper.mapCustomers(customerRepository.findAll());
  }
}