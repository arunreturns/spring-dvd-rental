
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.PaymentRepository;
import com.dvd.rental.rentalappl.dto.PaymentDto;
import com.dvd.rental.rentalappl.mapper.PaymentMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payment")
public class PaymentController {

  private PaymentRepository paymentRepositroy;

  @Autowired
  public PaymentController(PaymentRepository paymentRepositroy) {
    this.paymentRepositroy = paymentRepositroy;
  }

  @GetMapping("/")
  public List<PaymentDto> getItems() {
    return PaymentMapper.mapPayments(paymentRepositroy.findAll());
  }
}