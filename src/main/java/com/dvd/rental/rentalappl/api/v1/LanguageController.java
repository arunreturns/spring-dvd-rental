
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.LanguageRepository;
import com.dvd.rental.rentalappl.dto.LanguageDto;
import com.dvd.rental.rentalappl.mapper.LanguageMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/languages")
public class LanguageController {

  private LanguageRepository languageRepository;

  @Autowired
  public LanguageController(LanguageRepository languageRepository) {
    this.languageRepository = languageRepository;
  }

  @GetMapping("/")
  public List<LanguageDto> getItems() {
    return LanguageMapper.mapLanguages(languageRepository.findAll());
  }
}