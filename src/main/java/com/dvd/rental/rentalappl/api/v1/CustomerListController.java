package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.CustomerListRepository;
import com.dvd.rental.rentalappl.dto.CustomerListDto;
import com.dvd.rental.rentalappl.mapper.CustomerListMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customerList")
public class CustomerListController {

  private CustomerListRepository customerListRepository;

  @Autowired
  public CustomerListController(CustomerListRepository customerListRepository) {
    this.customerListRepository = customerListRepository;
  }

  @GetMapping("/")
  public List<CustomerListDto> getItems() {
    return CustomerListMapper.mapCustomerLists(customerListRepository.findAll());
  }
}