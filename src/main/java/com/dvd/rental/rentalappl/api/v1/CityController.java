
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.CityRepository;
import com.dvd.rental.rentalappl.dto.CityDto;
import com.dvd.rental.rentalappl.mapper.CityMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/city")
public class CityController {

  private CityRepository cityRepositroy;

  @Autowired
  public CityController(CityRepository cityRepositroy) {
    this.cityRepositroy = cityRepositroy;
  }

  @GetMapping("/")
  public List<CityDto> getItems() {
    return CityMapper.mapCities(cityRepositroy.findAll());
  }
}