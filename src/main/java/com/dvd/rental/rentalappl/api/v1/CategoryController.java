package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.CategoryRepository;
import com.dvd.rental.rentalappl.dto.CategoryDto;
import com.dvd.rental.rentalappl.mapper.CategoryMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryController {

  private CategoryRepository categoryRepository;

  @Autowired
  public CategoryController(CategoryRepository categoryRepository) {
    this.categoryRepository = categoryRepository;
  }

  @GetMapping("/")
  public List<CategoryDto> getItems() {
    return CategoryMapper.mapCategories(categoryRepository.findAll());
  }
}