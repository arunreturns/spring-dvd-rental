
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.ActorRepository;
import com.dvd.rental.rentalappl.dto.ActorDto;
import com.dvd.rental.rentalappl.mapper.ActorMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/actors")
public class ActorController {

  private ActorRepository actorRepository;

  @Autowired
  public ActorController(ActorRepository actorRepository) {
    this.actorRepository = actorRepository;
  }

  @GetMapping("/")
  public List<ActorDto> getItems() {
    return ActorMapper.mapActors(actorRepository.findAll());
  }
}