
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.FilmActorRepository;
import com.dvd.rental.rentalappl.dto.FilmActorDto;
import com.dvd.rental.rentalappl.mapper.FilmActorMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/filmActor")
public class FilmActorController {

  private FilmActorRepository filmActorRepositroy;

  @Autowired
  public FilmActorController(FilmActorRepository filmActorRepositroy) {
    this.filmActorRepositroy = filmActorRepositroy;
  }

  @GetMapping("/")
  public List<FilmActorDto> getItems() {
    return FilmActorMapper.mapFilmActors(filmActorRepositroy.findAll());
  }
}