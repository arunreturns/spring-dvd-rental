
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.FilmCategoryRepository;
import com.dvd.rental.rentalappl.dto.FilmCategoryDto;
import com.dvd.rental.rentalappl.mapper.FilmCategoryMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/filmCategory")
public class FilmCategoryController {

  private FilmCategoryRepository filmCategoryRepository;

  @Autowired
  public FilmCategoryController(FilmCategoryRepository filmCategoryRepository) {
    this.filmCategoryRepository = filmCategoryRepository;
  }

  @GetMapping("/")
  public List<FilmCategoryDto> getItems() {
    return FilmCategoryMapper.mapFilmCategories(filmCategoryRepository.findAll());
  }
}