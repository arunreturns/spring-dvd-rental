
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.StaffRepository;
import com.dvd.rental.rentalappl.dto.StaffDto;
import com.dvd.rental.rentalappl.mapper.StaffMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/staff")
public class StaffController {

  private StaffRepository staffRepositroy;

  @Autowired
  public StaffController(StaffRepository staffRepositroy) {
    this.staffRepositroy = staffRepositroy;
  }

  @GetMapping("/")
  public List<StaffDto> getItems() {
    return StaffMapper.mapStaffs(staffRepositroy.findAll());
  }
}