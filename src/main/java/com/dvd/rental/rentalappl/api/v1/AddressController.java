
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.AddressRepository;
import com.dvd.rental.rentalappl.dto.AddressDto;
import com.dvd.rental.rentalappl.mapper.AddressMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/address")
public class AddressController {

  private AddressRepository addressRepositroy;

  @Autowired
  public AddressController(AddressRepository addressRepositroy) {
    this.addressRepositroy = addressRepositroy;
  }

  @GetMapping("/")
  public List<AddressDto> getItems() {
    return AddressMapper.mapAddresses(addressRepositroy.findAll());
  }
}