
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.ActorInfoRepository;
import com.dvd.rental.rentalappl.dto.ActorInfoDto;
import com.dvd.rental.rentalappl.mapper.ActorInfoMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/actorInfo")
public class ActorInfoController {

  private ActorInfoRepository actorInfoRepository;

  @Autowired
  public ActorInfoController(ActorInfoRepository actorInfoRepository) {
    this.actorInfoRepository = actorInfoRepository;
  }

  @GetMapping("/")
  public List<ActorInfoDto> getItems() {
    return ActorInfoMapper.mapActorInfos(actorInfoRepository.findAll());
  }

  @GetMapping("/{firstName}/{lastName}")
  public ActorInfoDto findFilmsForActor(@PathVariable("firstName") String firstName,
      @PathVariable("lastName") String lastName) {
    return ActorInfoMapper.mapActorInfo(actorInfoRepository.findByFirstNameAndLastName(firstName, lastName));
  }

  @GetMapping("/{category}")
  public List<ActorInfoDto> findActorInFilmCategories(@PathVariable("category") String category) {
    return ActorInfoMapper.mapActorInfos(actorInfoRepository.findByFilmInfoContaining(category));
  }
}