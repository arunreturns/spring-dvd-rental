
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.FilmListRepository;
import com.dvd.rental.rentalappl.dto.FilmListDto;
import com.dvd.rental.rentalappl.mapper.FilmListMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/filmList")
public class FilmListController {

  private FilmListRepository filmListRepositroy;

  @Autowired
  public FilmListController(FilmListRepository filmListRepositroy) {
    this.filmListRepositroy = filmListRepositroy;
  }

  @GetMapping("/")
  public List<FilmListDto> getItems() {
    return FilmListMapper.mapFilmLists(filmListRepositroy.findAll());
  }
}