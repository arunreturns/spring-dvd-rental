
package com.dvd.rental.rentalappl.api.v1;

import java.util.List;

import com.dvd.rental.rentalappl.domain.repository.CountryRepository;
import com.dvd.rental.rentalappl.dto.CountryDto;
import com.dvd.rental.rentalappl.mapper.CountryMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/country")
public class CountryController {

  private CountryRepository countryRepositroy;

  @Autowired
  public CountryController(CountryRepository countryRepositroy) {
    this.countryRepositroy = countryRepositroy;
  }

  @GetMapping("/")
  public List<CountryDto> getItems() {
    return CountryMapper.mapCountries(countryRepositroy.findAll());
  }
}