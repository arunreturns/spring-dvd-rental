package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Actor;
import com.dvd.rental.rentalappl.dto.ActorDto;

public class ActorMapper {
  public static List<ActorDto> mapActors(List<Actor> actors) {
    return actors.stream().map(ActorMapper::mapActor).collect(Collectors.toList());
  }

  public static ActorDto mapActor(Actor actor) {
    ActorDto actorDto = new ActorDto();
    actorDto.setActorId(actor.getActorId());
    actorDto.setFirstName(actor.getFirstName());
    actorDto.setLastName(actor.getLastName());
    actorDto.setLastUpdate(actor.getLastUpdate());
    return actorDto;
  }
}