package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.FilmCategory;
import com.dvd.rental.rentalappl.dto.FilmCategoryDto;

public class FilmCategoryMapper {
  public static List<FilmCategoryDto> mapFilmCategories(List<FilmCategory> filmCategories) {
    return filmCategories.stream().map(FilmCategoryMapper::mapFilmCategory).collect(Collectors.toList());
  }

  public static FilmCategoryDto mapFilmCategory(FilmCategory filmCategory) {
    FilmCategoryDto filmCategoryDto = new FilmCategoryDto();
    filmCategoryDto.setCategoryId(filmCategory.getCategoryId());
    filmCategoryDto.setFilmId(filmCategory.getFilmId());
    filmCategoryDto.setLastUpdate(filmCategory.getLastUpdate());
    return filmCategoryDto;
  }
}