package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Customer;
import com.dvd.rental.rentalappl.dto.CustomerDto;

public class CustomerMapper {
  public static List<CustomerDto> mapCustomers(List<Customer> customers) {
    return customers.stream().map(CustomerMapper::mapCustomer).collect(Collectors.toList());
  }

  public static CustomerDto mapCustomer(Customer customer) {
    CustomerDto customerDto = new CustomerDto();
    customerDto.setCustomerId(customer.getCustomerId());
    customerDto.setAddress(AddressMapper.mapAddress(customer.getAddress()));
    customerDto.setActive(customer.getActive());
    customerDto.setActivebool(customer.getActivebool());
    customerDto.setCreateDate(customer.getCreateDate());
    customerDto.setLastUpdate(customer.getLastUpdate());
    customerDto.setEmail(customer.getEmail());
    customerDto.setFirstName(customer.getFirstName());
    customerDto.setLastName(customer.getLastName());
    return customerDto;
  }
}