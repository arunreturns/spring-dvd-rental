package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.FilmActor;
import com.dvd.rental.rentalappl.dto.FilmActorDto;

public class FilmActorMapper {
  public static List<FilmActorDto> mapFilmActors(List<FilmActor> filmActors) {
    return filmActors.stream().map(FilmActorMapper::mapFilmActor).collect(Collectors.toList());
  }

  public static FilmActorDto mapFilmActor(FilmActor filmActor) {
    FilmActorDto filmActorDto = new FilmActorDto();
    filmActorDto.setActorId(filmActor.getActorId());
    filmActorDto.setFilmId(filmActor.getFilmId());
    filmActorDto.setLastUpdate(filmActor.getLastUpdate());
    return filmActorDto;
  }
}