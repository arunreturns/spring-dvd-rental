package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Store;
import com.dvd.rental.rentalappl.dto.StoreDto;

public class StoreMapper {
  public static List<StoreDto> mapStores(List<Store> stores) {
    return stores.stream().map(StoreMapper::mapStore).collect(Collectors.toList());
  }

  public static StoreDto mapStore(Store store) {
    StoreDto storeDto = new StoreDto();
    storeDto.setStoreId(store.getStoreId());
    storeDto.setManagerStaffId(store.getManagerStaffId());
    storeDto.setAddress(AddressMapper.mapAddress(store.getAddress()));
    storeDto.setLastUpdate(store.getLastUpdate());
    return storeDto;
  }
}