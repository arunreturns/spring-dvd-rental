package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Address;
import com.dvd.rental.rentalappl.dto.AddressDto;

public class AddressMapper {
  public static List<AddressDto> mapAddresses(List<Address> addresses) {
    return addresses.stream().map(AddressMapper::mapAddress).collect(Collectors.toList());
  }

  public static AddressDto mapAddress(Address address) {
    AddressDto addressDto = new AddressDto();
    addressDto.setAddress(address.getAddress());
    addressDto.setAddress2(address.getAddress2());
    addressDto.setAddressId(address.getAddressId());
    addressDto.setCity(CityMapper.mapCity(address.getCity()));
    addressDto.setDistrict(address.getDistrict());
    addressDto.setPhone(address.getPhone());
    addressDto.setPostalCode(address.getPostalCode());
    addressDto.setLastUpdate(address.getLastUpdate());
    return addressDto;
  }
}