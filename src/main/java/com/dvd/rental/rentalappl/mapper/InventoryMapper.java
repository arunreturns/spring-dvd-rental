package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Inventory;
import com.dvd.rental.rentalappl.dto.InventoryDto;

public class InventoryMapper {
  public static List<InventoryDto> mapInventories(List<Inventory> inventories) {
    return inventories.stream().map(InventoryMapper::mapInventory).collect(Collectors.toList());
  }

  public static InventoryDto mapInventory(Inventory inventory) {
    InventoryDto inventoryDto = new InventoryDto();
    inventoryDto.setFilmId(inventory.getFilmId());
    inventoryDto.setInventoryId(inventory.getInventoryId());
    inventoryDto.setStoreId(inventory.getStoreId());
    inventoryDto.setLastUpdate(inventory.getLastUpdate());
    return inventoryDto;
  }
}