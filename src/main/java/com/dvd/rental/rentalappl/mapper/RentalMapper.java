package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Rental;
import com.dvd.rental.rentalappl.dto.RentalDto;

public class RentalMapper {
  public static List<RentalDto> mapRentals(List<Rental> rentals) {
    return rentals.stream().map(RentalMapper::mapRental).collect(Collectors.toList());
  }

  public static RentalDto mapRental(Rental rental) {
    RentalDto rentalDto = new RentalDto();
    rentalDto.setRentalId(rental.getRentalId());
    rentalDto.setRentalDate(rental.getReturnDate());
    rentalDto.setInventoryId(rental.getInventoryId());
    rentalDto.setCustomerId(rental.getCustomerId());
    rentalDto.setReturnDate(rental.getReturnDate());
    rentalDto.setStaffId(rental.getStaffId());
    rentalDto.setLastUpdate(rental.getLastUpdate());
    return rentalDto;
  }
}