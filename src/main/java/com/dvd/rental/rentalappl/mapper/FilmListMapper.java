package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.FilmList;
import com.dvd.rental.rentalappl.dto.FilmListDto;

public class FilmListMapper {
  public static List<FilmListDto> mapFilmLists(List<FilmList> filmLists) {
    return filmLists.stream().map(FilmListMapper::mapFilmList).collect(Collectors.toList());
  }

  public static FilmListDto mapFilmList(FilmList filmList) {
    FilmListDto filmListDto = new FilmListDto();
    filmListDto.setFid(filmList.getFid());
    filmListDto.setTitle(filmList.getTitle());
    filmListDto.setDescription(filmList.getDescription());
    filmListDto.setCategory(filmList.getCategory());
    filmListDto.setPrice(filmList.getPrice());
    filmListDto.setLength(filmList.getLength());
    filmListDto.setRating(filmList.getRating());
    filmListDto.setActors(filmList.getActors());
    return filmListDto;
  }
}