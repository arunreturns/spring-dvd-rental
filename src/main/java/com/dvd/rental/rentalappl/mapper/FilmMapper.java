package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Film;
import com.dvd.rental.rentalappl.dto.FilmDto;

public class FilmMapper {
  public static List<FilmDto> mapFilms(List<Film> films) {
    return films.stream().map(FilmMapper::mapFilm).collect(Collectors.toList());
  }

  public static FilmDto mapFilm(Film film) {
    FilmDto filmDto = new FilmDto();
    filmDto.setFilmId(film.getFilmId());
    filmDto.setTitle(film.getTitle());
    filmDto.setDescription(film.getDescription());
    filmDto.setReleaseYear(film.getReleaseYear());
    filmDto.setLanguage(LanguageMapper.mapLanguage(film.getLanguage()));
    filmDto.setRentalDuration(film.getRentalDuration());
    filmDto.setRentalRate(film.getRentalRate());
    filmDto.setLength(film.getLength());
    filmDto.setReplacementCost(film.getReplacementCost());
    filmDto.setRating(film.getRating());
    filmDto.setLastUpdate(film.getLastUpdate());
    filmDto.setSpecialFeatures(film.getSpecialFeatures());
    filmDto.setFulltext(film.getFulltext());
    return filmDto;
  }
}