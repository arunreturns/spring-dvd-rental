package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Category;
import com.dvd.rental.rentalappl.dto.CategoryDto;

public class CategoryMapper {
  public static List<CategoryDto> mapCategories(List<Category> categories) {
    return categories.stream().map(CategoryMapper::mapCategory).collect(Collectors.toList());
  }

  public static CategoryDto mapCategory(Category category) {
    CategoryDto categoryDto = new CategoryDto();
    categoryDto.setCategoryId(category.getCategoryId());
    categoryDto.setName(category.getName());
    categoryDto.setLastUpdate(category.getLastUpdate());
    return categoryDto;
  }
}