package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.City;
import com.dvd.rental.rentalappl.dto.CityDto;

public class CityMapper {
  public static List<CityDto> mapCities(List<City> cities) {
    return cities.stream().map(CityMapper::mapCity).collect(Collectors.toList());
  }

  public static CityDto mapCity(City city) {
    CityDto cityDto = new CityDto();
    cityDto.setCity(city.getCity());
    cityDto.setCityId(city.getCityId());
    cityDto.setCountry(CountryMapper.mapCountry(city.getCountry()));
    cityDto.setLastUpdate(city.getLastUpdate());
    return cityDto;
  }
}