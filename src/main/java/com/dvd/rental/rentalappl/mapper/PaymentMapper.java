package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Payment;
import com.dvd.rental.rentalappl.dto.PaymentDto;

public class PaymentMapper {
  public static List<PaymentDto> mapPayments(List<Payment> payments) {
    return payments.stream().map(PaymentMapper::mapPayment).collect(Collectors.toList());
  }

  public static PaymentDto mapPayment(Payment payment) {
    PaymentDto paymentDto = new PaymentDto();
    paymentDto.setPaymentId(payment.getPaymentId());
    paymentDto.setCustomerId(payment.getCustomerId());
    paymentDto.setStaffId(payment.getStaffId());
    paymentDto.setRentalId(payment.getRentalId());
    paymentDto.setAmount(payment.getAmount());
    paymentDto.setPaymentDate(payment.getPaymentDate());
    return paymentDto;
  }
}