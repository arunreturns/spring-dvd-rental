package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.ActorInfo;
import com.dvd.rental.rentalappl.dto.ActorInfoDto;

public class ActorInfoMapper {
  public static List<ActorInfoDto> mapActorInfos(List<ActorInfo> actorInfos) {
    return actorInfos.stream().map(ActorInfoMapper::mapActorInfo).collect(Collectors.toList());
  }

  public static ActorInfoDto mapActorInfo(ActorInfo actorInfo) {
    ActorInfoDto actorInfoDto = new ActorInfoDto();
    actorInfoDto.setActorId(actorInfo.getActorId());
    actorInfoDto.setFirstName(actorInfo.getFirstName());
    actorInfoDto.setLastName(actorInfo.getLastName());
    actorInfoDto.setFilmInfo(actorInfo.getFilmInfo());
    return actorInfoDto;
  }
}