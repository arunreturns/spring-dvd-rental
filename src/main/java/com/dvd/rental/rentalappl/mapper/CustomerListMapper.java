package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.CustomerList;
import com.dvd.rental.rentalappl.dto.CustomerListDto;

public class CustomerListMapper {
  public static List<CustomerListDto> mapCustomerLists(List<CustomerList> customerLists) {
    return customerLists.stream().map(CustomerListMapper::mapCustomerList).collect(Collectors.toList());
  }

  public static CustomerListDto mapCustomerList(CustomerList customerList) {
    CustomerListDto customerListDto = new CustomerListDto();
    customerListDto.setId(customerList.getId());
    customerListDto.setName(customerList.getName());
    customerListDto.setAddress(customerList.getAddress());
    customerListDto.setZipCode(customerList.getZipCode());
    customerListDto.setPhone(customerList.getPhone());
    customerListDto.setCity(customerList.getCity());
    customerListDto.setCountry(customerList.getCountry());
    customerListDto.setNotes(customerList.getNotes());
    customerListDto.setSid(customerList.getSid());
    return customerListDto;
  }
}