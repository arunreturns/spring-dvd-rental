package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Staff;
import com.dvd.rental.rentalappl.dto.StaffDto;

public class StaffMapper {
  public static List<StaffDto> mapStaffs(List<Staff> staffs) {
    return staffs.stream().map(StaffMapper::mapStaff).collect(Collectors.toList());
  }

  public static StaffDto mapStaff(Staff staff) {
    StaffDto staffDto = new StaffDto();
    staffDto.setStaffId(staff.getStaffId());
    staffDto.setFirstName(staff.getFirstName());
    staffDto.setLastName(staff.getLastName());
    staffDto.setAddress(AddressMapper.mapAddress(staff.getAddress()));
    staffDto.setEmail(staff.getEmail());
    staffDto.setStore(StoreMapper.mapStore(staff.getStore()));
    staffDto.setActive(staff.getActive());
    staffDto.setUsername(staff.getUsername());
    staffDto.setPicture(staff.getPicture());
    staffDto.setLastUpdate(staff.getLastUpdate());
    return staffDto;
  }
}