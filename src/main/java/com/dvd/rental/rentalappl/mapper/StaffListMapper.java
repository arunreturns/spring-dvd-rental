package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.StaffList;
import com.dvd.rental.rentalappl.dto.StaffListDto;

public class StaffListMapper {
  public static List<StaffListDto> mapStaffLists(List<StaffList> staffLists) {
    return staffLists.stream().map(StaffListMapper::mapStaffList).collect(Collectors.toList());
  }

  public static StaffListDto mapStaffList(StaffList staffList) {
    StaffListDto staffListDto = new StaffListDto();
    staffListDto.setId(staffList.getId());
    staffListDto.setName(staffList.getName());
    staffListDto.setAddress(staffList.getAddress());
    staffListDto.setZipCode(staffList.getZipCode());
    staffListDto.setPhone(staffList.getPhone());
    staffListDto.setCity(staffList.getCity());
    staffListDto.setCountry(staffList.getCountry());
    staffListDto.setSid(staffList.getSid());
    return staffListDto;
  }
}