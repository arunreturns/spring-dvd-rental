package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Language;
import com.dvd.rental.rentalappl.dto.LanguageDto;

public class LanguageMapper {
  public static List<LanguageDto> mapLanguages(List<Language> languages) {
    return languages.stream().map(LanguageMapper::mapLanguage).collect(Collectors.toList());
  }

  public static LanguageDto mapLanguage(Language language) {
    LanguageDto languageDto = new LanguageDto();
    languageDto.setLanguageId(language.getLanguageId());
    languageDto.setName(language.getName().trim());
    languageDto.setLastUpdate(language.getLastUpdate());
    return languageDto;
  }
}