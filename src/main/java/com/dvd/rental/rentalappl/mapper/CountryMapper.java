package com.dvd.rental.rentalappl.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.dvd.rental.rentalappl.domain.model.Country;
import com.dvd.rental.rentalappl.dto.CountryDto;

public class CountryMapper {

  public static List<CountryDto> mapCountries(List<Country> countries) {
    return countries.stream().map(CountryMapper::mapCountry).collect(Collectors.toList());
  }

  public static CountryDto mapCountry(Country country) {
    CountryDto countryDto = new CountryDto();
    countryDto.setCountryId(country.getCountryId());
    countryDto.setCountry(country.getCountry());
    countryDto.setLastUpdate(country.getLastUpdate());
    return countryDto;
  }
}