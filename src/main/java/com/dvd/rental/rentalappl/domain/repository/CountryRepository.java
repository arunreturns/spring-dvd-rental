package com.dvd.rental.rentalappl.domain.repository;

import java.util.List;

import com.dvd.rental.rentalappl.domain.model.Country;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends CrudRepository<Country, Long> {
  @Override
  List<Country> findAll();
}