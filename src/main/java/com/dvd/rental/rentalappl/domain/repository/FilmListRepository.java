package com.dvd.rental.rentalappl.domain.repository;

import java.util.List;

import com.dvd.rental.rentalappl.domain.model.FilmList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmListRepository extends CrudRepository<FilmList, Long> {
  @Override
  List<FilmList> findAll();
}