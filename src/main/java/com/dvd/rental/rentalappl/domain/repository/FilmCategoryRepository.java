package com.dvd.rental.rentalappl.domain.repository;

import java.util.List;

import com.dvd.rental.rentalappl.domain.model.FilmCategory;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmCategoryRepository extends CrudRepository<FilmCategory, Long> {
  @Override
  List<FilmCategory> findAll();
}