package com.dvd.rental.rentalappl.domain.repository;

import java.util.List;

import com.dvd.rental.rentalappl.domain.model.ActorInfo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorInfoRepository extends CrudRepository<ActorInfo, Long> {
  @Override
  List<ActorInfo> findAll();

  ActorInfo findByFirstNameAndLastName(String firstName, String lastName);

  List<ActorInfo> findByFilmInfoContaining(String category);
}