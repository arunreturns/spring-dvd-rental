package com.dvd.rental.rentalappl.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
@Entity
public class Film implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  private Long filmId;
  private String title;
  private String description;
  private String releaseYear;
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "language_id", insertable = false, updatable = false)
  @Fetch(FetchMode.JOIN)
  private Language language;
  private Integer rentalDuration;
  private Double rentalRate;
  private Integer length;
  private Double replacementCost;
  private String rating;
  private Date lastUpdate;
  private String specialFeatures;
  private String fulltext;
}