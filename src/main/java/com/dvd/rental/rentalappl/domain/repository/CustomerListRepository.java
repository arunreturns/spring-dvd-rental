package com.dvd.rental.rentalappl.domain.repository;

import java.util.List;

import com.dvd.rental.rentalappl.domain.model.CustomerList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerListRepository extends CrudRepository<CustomerList, Long> {
  @Override
  List<CustomerList> findAll();
}