package com.dvd.rental.rentalappl.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
@Entity
public class Rental implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  private Long rentalId;
  private Date rentalDate;
  private Long inventoryId;
  private Long customerId;
  private Date returnDate;
  private Long staffId;
  private Date lastUpdate;
}