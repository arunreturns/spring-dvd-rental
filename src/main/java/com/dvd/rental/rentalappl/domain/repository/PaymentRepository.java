package com.dvd.rental.rentalappl.domain.repository;

import java.util.List;

import com.dvd.rental.rentalappl.domain.model.Payment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Long> {
  @Override
  List<Payment> findAll();
}