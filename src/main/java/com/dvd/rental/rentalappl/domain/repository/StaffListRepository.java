package com.dvd.rental.rentalappl.domain.repository;

import java.util.List;

import com.dvd.rental.rentalappl.domain.model.StaffList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffListRepository extends CrudRepository<StaffList, Long> {
  @Override
  List<StaffList> findAll();
}