package com.dvd.rental.rentalappl.domain.repository;

import java.util.List;

import com.dvd.rental.rentalappl.domain.model.FilmActor;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmActorRepository extends CrudRepository<FilmActor, Long> {
  @Override
  List<FilmActor> findAll();
}