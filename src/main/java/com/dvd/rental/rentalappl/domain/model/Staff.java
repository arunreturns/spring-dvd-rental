package com.dvd.rental.rentalappl.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
@Entity
public class Staff implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  private Long staffId;
  private String firstName;
  private String lastName;
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "address_id", insertable = false, updatable = false)
  @Fetch(FetchMode.JOIN)
  private Address address;
  private String email;
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "store_id", insertable = false, updatable = false)
  @Fetch(FetchMode.JOIN)
  private Store store;
  private Boolean active;
  private String username;
  private String password;
  private Date lastUpdate;
  private String picture;
}