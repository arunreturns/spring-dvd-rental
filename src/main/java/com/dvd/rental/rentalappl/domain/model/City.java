package com.dvd.rental.rentalappl.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
@Entity
public class City implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  private Long cityId;
  private String city;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "country_id", insertable = false, updatable = false)
  @Fetch(FetchMode.JOIN)
  private Country country;
  private Date lastUpdate;
}